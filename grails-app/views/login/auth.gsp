<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<head>

   <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <asset:stylesheet src="kc.css" />

</head>
<body class="blank">


<div class="color-line"></div>



<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
            </div>

            <div class="hpanel">
                <div class="panel-body">
                        
                        <form class="form-signin" action='${postUrl}' method='POST' id='loginForm' class='cssform' autocomplete='off'>
                            <div class="form-group">
                                <label class="control-label" for="username">Username</label>
                                <input type="text" placeholder="" title="Please enter you username" required="" value="" name='j_username' id='username' class="form-control">
<!--                                <span class="help-block small">Your unique username to app</span>-->
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="password">Password</label>
                                <input type="password" title="Please enter your password" placeholder="******" required="" value="" name='j_password' id='password' class="form-control">
<!--                                <span class="help-block small">Yur strong password</span>-->
                            </div>
                            <div class="checkbox">
                                <input type="checkbox" class="i-checks" checked name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/>
                                     Remember me
                                <p class="help-block small"></p>
                            </div>
                            <button  type='submit' id="submit" class="btn btn-primary btn-block">Login</button>
                            <g:if test='${flash.message}'>
								<div style="color:red" class='login_message'>${flash.message}</div>
							</g:if>
                            <!--<div class="forgotpwd"><a class="forgotpwdLink" href="${createLink(controller: 'login', action:'forgotPassword')}">Forgot Password?</a></div>
                            <a class="btn btn-default btn-block" href="#">Register</a>-->
                        </form>
                </div>
            </div>
        </div>
    </div>
<!--    <div class="row admin">
        <a class="adminLogin" href="${postUrl}">Admin Login</a>
    </div>-->
        
</div>
 
<footer>
    <div class="row">
        <div class="col-md-12 text-center copy-right">
            Copyright 2015, <span><strong>My Inc.</strong></span> All rights reserved
        </div>
    </div>
    <div class="color-line"></div>
    <div class="footer-line"></div>
    
</footer>

</body>
</html>