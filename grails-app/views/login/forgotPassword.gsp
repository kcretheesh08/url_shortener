<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>Compumail - Web App</title>

  
	<asset:stylesheet src="/fontawesome/css/font-awesome.css" />
    <asset:stylesheet src="/bootstrap/dist/css/bootstrap.css" />
	<asset:stylesheet src="style.css"/>
	
	<asset:javascript src="/jquery/dist/jquery.min.js"/>
	<asset:javascript src="/bootstrap/dist/js/bootstrap.min.js"/>
	

</head>
<body class="blank">
<div class="successBox forgot-success">
    <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Success - </strong> Check your inbox to retrieve </br> your password. (<a href="#">Inbox Link</a>)
    </div>
</div>

<div class="color-line"></div>



<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <asset:image src="logo.jpg" alt="Grails"/>
            </div>

            <div class="hpanel">
                <div class="panel-body">
                        <g:if test='${flash.message}'>
							<div style="color:red" class='login_message'>${flash.message}</div>
						</g:if>
                        <form  action='${postUrl}' method='POST' id='loginForm' class='cssform' autocomplete='off'>
                            <div class="forgotLabel"><h4>Forgot Password?</h4></div>
                            
                            <div class="form-group">
                                <label class="control-label" for="username">Enter your email</label>
                                <input type="text" placeholder="" title="Please enter you username" required="" value="" name='j_username' id='username' class="form-control">
<!--                                <span class="help-block small">Your unique username to app</span>-->
                            </div>
                            
                            
                            <button  type='submit' id="submit" class="btn btn-primary btn-block">Submit</button>
                            <div class="login">If you remember your password,<a class="loginLink" href="${createLink(controller: 'Login', action:'index')}"> login.</a></div>
                            <!--<a class="btn btn-default btn-block" href="#">Register</a>-->
                        </form>
                </div>
            </div>
        </div>
    </div>
    
        
</div>
 
<footer>
    <div class="row">
        <div class="col-md-12 text-center copy-right">
            Copyright 2015, <span><strong>Compumail Inc.</strong></span> All rights reserved
        </div>
    </div>
    <div class="color-line"></div>
    <div class="footer-line"></div>
    
</footer>
<script>
$(document).ready(function(){
 
    $('#submit').on('click',function(){
        $('.successBox').show();
    });

});

</script>

</body>
</html>