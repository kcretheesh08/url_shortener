<!DOCTYPE html>
<html lang="en">
<head>
  <title>URL Shorten Service</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<asset:stylesheet src="kc.css" />
</head>
<body>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>URL Shortener</h4>
      <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href="#section1">Home</a></li>
        <li><a href="#section3">About</a></li>
        <li class="dropdown"><a	HREF="javascript:document.submitForm.submit()">
							<form name="submitForm" method="POST" action="${createLink(controller: 'logout')}">
								<input type="hidden" name="" value="">Logout
							</form> <i class="pe-7s-upload pe-rotate-90"></i>
					</a></li>
      </ul><br>
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search ..">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">
            <span class="glyphicon glyphicon-search"></span>
          </button>
        </span>
      </div>
    </div>

    <div class="col-sm-9">
      <h1>URL Shorten</h1>
       <form>
  <div class="form-row">
    <div class="form-group col-md-8">
      <label for="inputUrl">Enter URL</label>
      <input type="text" class="form-control" id="inputUrl" placeholder="URL">
      <div style="color:red; display: none;" class='error_message' id=error_message> </div>
    </div>
    
  </div>
 
	<div class="form-row">
    	<div class="form-group col-md-8">
			<button type="submit" class="btn btn-success" id="shortUrl" >Shorten a long URL</button>
			<button type="submit" class="btn btn-success pull-right" id="expandUrl" >Expand a short URL</button>
		</div>
	</div>
	
		<div class="" id="outputTxtDiv" >
    	<div class="form-group col-md-8">
			<div class="alert alert-info" role="alert">
			  <strong> <span id="outputTxt"> </span> </strong>
			</div>
		</div>
		</div>
</form>

      <br><br>
    </div>
  </div>
</div>

<script type="text/javascript">


function getBaseUrl() {
	var location = window.location;
	var base_url="";
	var mypath = "/urlShortener";
	//if(mypath.length > 0)
		//mypath = mypath.substring(0, mypath.length-1);
	base_url = location.protocol + "//" + location.host + mypath ;

	console.log("base_url " + base_url);
	return base_url;
}

function disableButtons() {
	$('#shortUrl').attr('disabled', 'disabled');
	$('#expandUrl').attr('disabled', 'disabled');
}

function enableButtonsbyRemoveAttr() {
	$('#shortUrl').removeAttr("disabled");
	$('#expandUrl').removeAttr("disabled");
}

function doValidation() {
	var inputString =  $("#inputUrl").val();
	if (inputString == null || inputString == '') {
		$("#error_message").css({'display':'block'}); 
		$("#error_message").text( 'URL cannot be empty.' );
		return false;
	}
	return true;
}

$( document ).ready(function() {
	$('#outputTxtDiv').css('display','none');
	//alert("sss > " + getBaseUrl() );
	console.log("getBaseUrl() " + getBaseUrl());
});


$('#shortUrl').on('click',function(e){
	e.preventDefault();
	var url = getBaseUrl() + "/dashboard/shortenURL";
	var inputString =  $("#inputUrl").val();
	if(!doValidation()) {
		return false;
	}
	disableButtons();
	
	$.ajax({
		url:url,
		method: 'GET',
		contentType: 'application/json' ,
		data : { paramString : inputString },
		success: function(data) {
			console.log("asdasd " + data.id);
			//if(undefined != data.body){}
				$("#outputTxtDiv").css({'display':'block'}); 
				$('#outputTxt').text( data.id );

				enableButtonsbyRemoveAttr();
		},
		error: function(xhr){
			enableButtonsbyRemoveAttr();
			alert("Exception occured.");
		}
	});
});

$('#expandUrl').on('click',function(e){
	e.preventDefault();
	if(!doValidation()) {
		return false;
	}
	var url = getBaseUrl() + "/dashboard/expandURL";
	var inputString =  $("#inputUrl").val();

	disableButtons();
	
	$.ajax({
		url:url,
		method: 'GET',
		contentType: 'application/json' ,
		data : { paramString : inputString },
		success: function(data) {
			console.log("longURL " + data.longUrl);
			//if(undefined != data.body){}
				$("#outputTxtDiv").css({'display':'block'}); 
				$('#outputTxt').text( data.longUrl );
				enableButtonsbyRemoveAttr();
		},
		error: function(xhr){
			enableButtonsbyRemoveAttr();
			alert("Exception occured.");
		}
	});
});
	
</script>
</body>
</html>

