package com.util.rest

import com.app.constants.AppConstant;

import grails.transaction.Transactional
import grails.converters.JSON;
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse


@Transactional
class RestApiService {

	/**
	 * Service method to get short URL.
	 * @param urlString
	 * @return
	 */
	def getShortenUrl(def urlString){
		println "calling getShortenUrl service " + urlString

//		def url = "https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyARKHx5IfDM8K88kh9xZq_oYUzvzUpLjhs"
		def url = AppConstant.GOOGLE_API_SHORTEN + "?key=" + AppConstant.GOOGLE_API_KEY
		println "url " + url

		def jsonData = [:]
		jsonData.longUrl =  urlString
		
		println "jsonData : " + jsonData // as JSON
		
		def returnData = ["status":false, "data":null]
		RestBuilder rest = new RestBuilder()

		try {
				RestResponse resp = rest.post(url) {
					contentType "application/json"
					header 'Content-Type', 'application/json'
					json {
					  longUrl = urlString
					}
				}
			def json = resp.json

			println "Return JSON Data >> " + json

			if(resp.status == 200){
				returnData = ["status":true, "body":resp.json, "code": resp.status]
			} else {
				returnData = ["status":false, "body":resp.json, "code": resp.status]
			}
		}catch(Exception e){
		}
		
		/*def res = [kind:'urlshortener#url', longUrl:'http://www.google.com/', id:'https://goo.gl/fbsS']
			returnData = ["status":true, "body":res, "code": 200]*/
			
		println "Return Data >>> " + returnData
		returnData 
	}
	
	
	/**
	 * Service method to get expansion of short URL. 
	 * @param urlString
	 * @return
	 */
	def getLongUrl(def urlString){
		println "calling getLongUrl service with param " + urlString

		def url = AppConstant.GOOGLE_API_SHORTEN + "?key=" + AppConstant.GOOGLE_API_KEY + "&shortUrl=" + urlString

		def returnData = ["status":false, "data":null]
		RestBuilder rest = new RestBuilder()

		try {
				def resp = rest.get(url) {
//					contentType "application/json"
//					header 'Content-Type', 'application/json'
				}
			def json = resp.json
			println "Return JSON Data >> " + json

			if(resp.status == 200){
				returnData = ["status":true, "body":resp.json, "code": resp.status]
			} else {
				returnData = ["status":false, "body":resp.json, "code": resp.status]
			}
		}catch(Exception e){
		}
		
		println "Return Data >>> " + returnData
		returnData
	}
	
    def serviceMethod() {

    }
}
