package com

import grails.converters.JSON;
import grails.plugin.springsecurity.annotation.Secured;


@Secured(['ROLE_ADMIN'])
class DashboardController {

	def restApiService
    def index() { }
	
	def shortenURL() {
		log.info "Entering shortenURL with params " + params
		def dbUrl = UrlDetails.findByLongUrl(params.paramString)
		
		def data = [:]
		if(!dbUrl) {
			def resData = restApiService.getShortenUrl(params.paramString)
			if(resData.status) {
				try {
					def newUrl = new UrlDetails()
					newUrl.longUrl = params.paramString
					newUrl.shortUrl = resData.body.id
					newUrl.save(flush: true, failOnError: true)
				} catch (Exception e) {
					e.printStackTrace()
				}
				
				data.id = resData.body.id
				data.status = true
			} else {
				data.status = false
				data.id = "Can't able to convert URL. (Error type : $resData.code )"
			}
		} else {
			data.id = dbUrl.shortUrl
		}
		
		render data as JSON
	}
	
	def expandURL() {
		log.info "Entering expandURL with params " + params
		def dbUrl = UrlDetails.findAllByShortUrl(params.paramString)
		
		def data = [:]
		if(!dbUrl) {
			def resData = restApiService.getLongUrl(params.paramString)
			if(resData.status) {
				def newUrl = new UrlDetails()
				newUrl.longUrl =  resData.body.longUrl
				newUrl.shortUrl = params.paramString
	//			newUrl.save(flush: true)
				data.longUrl = resData?.body?.longUrl
			} else {
				data.status = false
				data.longUrl = resData?.body?.error.message
			}
		} else {
			data.longUrl = dbUrl[0].longUrl
		}
		render data as JSON
	}
}
