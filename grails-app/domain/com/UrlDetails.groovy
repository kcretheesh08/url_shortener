package com

class UrlDetails {

	String longUrl
	String shortUrl
	
    static constraints = {
		longUrl nullable: true
		shortUrl nullable: true
    }
}
